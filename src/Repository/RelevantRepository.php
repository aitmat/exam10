<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Relevant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Relevant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Relevant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Relevant[]    findAll()
 * @method Relevant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelevantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Relevant::class);
    }

    /**
    * @return Relevant[] Returns an array of Division objects
    */
    public function findByUser($user)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.user = :user')
            ->setParameter('user', $user)
            ->orderBy('d.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Division
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}